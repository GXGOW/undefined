const fs = require('fs');
const _ = require('lodash');

let cars = [];
const routes = [];
let grid = {};

fs.readFile('./input/a_example.in', 'utf8', (err, data) => {
    const input = data.split('\n');
    const firstLine = input[0].split(' ');
    grid = {
        row: Number.parseInt(firstLine[0]),
        column: Number.parseInt(firstLine[1])
    }
    const amountOfVehicels = Number.parseInt(firstLine[2]);
    const numberOfRides = Number.parseInt(firstLine[3]);
    const rideBonus = Number.parseInt(firstLine[4]);
    let totalSteps = Number.parseInt(firstLine[5]);
    for (let i = 1; i <= numberOfRides; i++) {
        const line = input[i].split(' ');
        routes.push({
            start: [Number.parseInt(line[0]), Number.parseInt(line[1])],
            end: [Number.parseInt(line[2]), Number.parseInt(line[3])],
            earliestStart: Number.parseInt(line[4]),
            latestFinish: Number.parseInt(line[5]),
            taken: false,
            index: i - 1,
        })
    }
    for (let i = 1; i <= amountOfVehicels; i++) {
        cars.push({
            location: [0, 0],
            stepsToDest: 0,
            routes: []
        });
    }
    for (let step = 0; step <= totalSteps; step++) {
        console.log(step + ' of ' + totalSteps);
        cars = _.forEach(cars, car => {
            if (car.currentRide) {
                if (car.currentRide.earliestStart <= step) {
                    if (car.location[0] < car.currentRide.end[0]) {
                        car.location = [car.location[0] + 1, car.location[1]]
                    } else if (car.location[0] > car.currentRide.end[0]) {
                        car.location = [car.location[0] - 1, car.location[1]]
                    } else {
                        if (car.location[1] < car.currentRide.end[1]) {
                            car.location = [car.location[0], car.location[1] + 1]
                        } else if (car.location[1] > car.currentRide.end[1]) {
                            car.location = [car.location[0], car.location[1] - 1]
                        } else {
                            findNewRide(car, step);
                        }
                    }
                }
            } else {
                findNewRide(car, step);
            }
        })
    }
    console.log('end');
    let output = '';
    _.forEach(cars, car => {
        output += _.size(car.routes) + ' ' + _.join(car.routes, ' ') + '\n';
    })
    fs.writeFile('output_a.txt', output, 'utf8', err => {
        if (err) {
            console.log('Lap tis naar de zak ' + err);
        }
    })
});

function findNewRide(car, step) {
    let smallestDistance = Number.MAX_SAFE_INTEGER;
    let closestRide = {};
    let routeIndex = 0;
    _.forEach(routes, (route, index) => {
        const distance = Math.abs(car.location[0] - route.start[0]) + Math.abs(car.location[1] - route.start[1]);
        const routeDistance = Math.abs(route.start[0] - route.end[0]) + Math.abs(route.start[1] - route.end[1]);
        const timeToWait = Math.max(step + distance - route.earliestStart, 0);
        if (smallestDistance > distance + timeToWait && step + distance + routeDistance <= route.latestFinish && route.taken === false) {
            smallestDistance = distance + timeToWait;
            closestRide = route;
            routeIndex = index;
        }
    });
    if (_.get(closestRide, 'start')) {
        closestRide.taken = true;
        car.currentRide = closestRide;
        car.routes.push(closestRide.index);
    }
};